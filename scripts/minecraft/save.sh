#!/bin/sh
# save.sh - backups Minecraft folder to a public folder for users to download from.

world='/srv/minecraft/survival/world'
folder='/public'
name='save'

cd "$folder"
zip -9r "$name.zip.new"
rm "$name.zip"
mv "$name.zip.new" "$name.zip"
