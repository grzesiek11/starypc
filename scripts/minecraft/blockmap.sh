#!/bin/bash
# blockmap.sh - BlockMap wrapper script.

# TODO
# - POSIX rewrite
# - config
# - more features


java=~/programy/java-13/bin/java
jar=~/BlockMap-cli-1.6.1.jar
output=/var/www/html/pages/minecraft/tdmap
world=/srv/minecraft/survival/world
dims=(OVERWORLD NETHER END)

for dim in ${dims[@]}; do
    "$java" -jar "$jar" render -o "$world/$dim" -d $dim --create-tile-html -l "$output"
done
