#!/bin/bash
# papyru.sh - PapyrusC# wrapper script.

# TODO
# - POSIX rewrite
# - more features
# - config file

papyruscmd="$HOME/programy/papyrus/PapyrusCs"
output="/var/www/html/pages/minecraft/bemap"
world="/srv/sidchroot/home/grzesiek11/bedrock/worlds/Lody Vanillowe"
dims=(0 1 2)
html="index.html"
opts=("--threads 1" "--maxqueue 1")

for dim in ${dims[@]}; do
    "$papyruscmd" -w "$world" -o "$output" -d $dim --htmlfile "$html" ${opts[@]}
done
