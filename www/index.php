<?php
$pageDir = 'pages';
$mainPage = 'main.html';
$pageArg = 'p';

function loadPage($location) {
    global $pageDir;

    $dirPath = realpath(dirname("$pageDir/$location"));
    $base = getcwd()."/$pageDir";

    if (substr($dirPath, 0, strlen($base)) == $base) {
        if (file_exists("$pageDir/$location")) {
            include("$pageDir/$location");
        } else {
            include("$pageDir/error/404.html");
        }
    } else {
        include("$pageDir/error/401.html");
    }
}?>

<html>
<head>
    <title>starypc</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="res/style.css">
    <link rel="icon" href="res/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="res/favicon.ico" type="image/x-icon">
</head>
<body>
    <div class="legacy-box">
        Ta strona jest stara i nie jest już aktualizowana. <a href="https://stary.pc.pl">Przejdź do obecnej strony...</a>
    </div>
    <div id="everything-container">
<?php
loadPage('header.html');

if (isset($_GET[$pageArg])) {
    if ($_GET[$pageArg] == '') {
        $page = $mainPage;
    } else {
        $page = $_GET[$pageArg];
    }
} else {
    $page = $mainPage;
}

loadPage($page);

loadPage('footer.html');
?>
    </div>
</body>
</html>