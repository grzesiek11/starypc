<h2>Status serwera</h2>

<h3>IP</h3>
<pre><?=file_get_contents('https://api.ipify.org')?></pre>

<h3>Uptime</h3>
<pre><?=shell_exec("tuptime")?></pre>

<h3>Użycie RAMu</h3>
<pre><?=shell_exec("free -h")?></pre>

<h3>Użycie dysku</h3>
<pre><?=shell_exec("df -h")?></pre>
